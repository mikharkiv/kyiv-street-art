## How to install:

### Step 1. Install Python and Node.js if you don't have one.

### Step 2. Install React

`npm install react`

### Step 3. Install dependencies:

`pip install -r requirements.txt`

`python manage.py migrate`

### Step 4. Create an admin user

`python manage.py createsuperuser`

### Step 5. Run your backend and frontend

`python manage.py runserver`

`npx start`

### Step 6. Congrats! Open your [app](http://localhost:3000/) and have fun!