from rest_framework import viewsets

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import *
from .serializers import *

class ArtworksViewSet(viewsets.ModelViewSet):
	queryset = Artwork.objects.all()
	serializer_class = ArtworkSerializer


class GenreViewSet(viewsets.ModelViewSet):
	queryset = Genre.objects.all()
	serializer_class = GenreSerializer


class TypeViewSet(viewsets.ModelViewSet):
	queryset = Type.objects.all()
	serializer_class = TypeSerializer


class ArtworkImageViewSet(viewsets.ModelViewSet):
	queryset = ArtworkImage.objects.all()
	serializer_class = ArtworkImageSerializer