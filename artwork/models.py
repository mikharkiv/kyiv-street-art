from django.db import models
from django.utils.translation import gettext_lazy as _
from datetime import datetime
from rest_framework import serializers


class ArtworkImage(models.Model):

	def get_upload_path(self, filename):
		return "artwork/%s/images/" % (str(datetime.now().strftime("%Y-%m-%d_%H-%M-%S"))) + filename

	id = serializers.ReadOnlyField()
	image = models.ImageField(upload_to=get_upload_path)


class Type(models.Model):
	id = serializers.ReadOnlyField()
	name = models.CharField(max_length=50)

	def __str__(self):
		return self.name


class Genre(models.Model):
	id = serializers.ReadOnlyField()
	name = models.CharField(max_length=70)
	description = models.TextField(blank=True)
	type = models.ForeignKey(Type, on_delete=models.CASCADE, blank=True, related_name='genre')

	def __str__(self):
		return self.name


class Artwork(models.Model):

	STATES = [
		('NO', _('Normal')),
		('UT', _('Under threat')),
		('DE', _('Destroyed')),
		('BC', _('In a bad condition')),
	]

	id = serializers.ReadOnlyField()
	name = models.CharField(max_length=50, blank=True)
	address = models.CharField(max_length=80)
	location_x = models.FloatField()
	location_y = models.FloatField()
	description = models.TextField()
	created = models.CharField(max_length=50, blank=True)
	added = models.DateTimeField(default=datetime.now, blank=True)
	photos = models.ManyToManyField('ArtworkImage', blank=True)
	type = models.ForeignKey(Type, on_delete=models.CASCADE, blank=True, related_name='types')
	genre = models.ManyToManyField('Genre', blank=True, related_name='genres')
	author = models.ManyToManyField('authors.Author', blank=True)
	state = models.CharField(max_length=2, choices=STATES, default='NO')

	def __str__(self):
		return self.name

