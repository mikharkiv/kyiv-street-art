from rest_framework import serializers
from.models import Artwork, ArtworkImage, Type, Genre

class ArtworkSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = Artwork
		fields=('id', 'name','address','location_x','location_y','description','created','added','photos','type','genre','author','state')


class GenreSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model =	Genre
		fields =('id', 'name', 'description', 'type')


class TypeSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = Type
		fields = ('id', 'name',)


class ArtworkImageSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = ArtworkImage
		fields = ('id', 'image',)
