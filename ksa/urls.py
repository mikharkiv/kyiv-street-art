from django.conf.urls import include, url
from django.urls import path, re_path
from django.contrib import admin
from rest_framework import routers
from artwork.views import *
from authors.views import *
from django.conf import settings
from django.conf.urls.static import static
from .views import index

router = routers.DefaultRouter()
router.register(r'artworks', ArtworksViewSet)
router.register(r'art-images', ArtworkImageViewSet)
router.register(r'types', TypeViewSet)
router.register(r'genres', GenreViewSet)
router.register(r'author-images', AuthorImageSet)
router.register(r'authors', AuthorViewSet)

urlpatterns = [
    path('', index, name='index'),
    url(r'api/', include(router.urls)),
    url(r'admin/', admin.site.urls),
    url(r'api/artworks/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'api/art-images/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'api/types/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'api/genres/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'api/author-images/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'api/authors/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^webpush/', include('webpush.urls')),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)