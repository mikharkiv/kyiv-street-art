import React from 'react';
import '../styles/App.css';
import Header from "./Header"
import Footer from "./Footer"
import HomePage from "./HomePage"
import GalleryPage from "./GalleryPage";
import MapPage from "./MapPage"	
import {Route, Switch} from 'react-router-dom'
import ArtistPage from './ArtistPage'
import ArtworkPage from './ArtworkPage';

function App() {
	return (
		<div className="App">
			<link
				rel="stylesheet"
				href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
				integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
				crossOrigin=""
			/>
			<Header/>
			<Switch>
				<Route path='/' component={MapPage} exact/>
				<Route path='/map' component={MapPage}/>
				<Route path='/gallery' component={GalleryPage}/>
				<Route path='/artist/:id' component={ArtistPage}/>
				<Route path='/artwork/:id' component={ArtworkPage}/>
			</Switch>
			<Footer/>
		</div>
	);
}

export default App;
