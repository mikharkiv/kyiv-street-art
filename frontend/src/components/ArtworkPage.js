import React from 'react';
import Tags from './Tags'
import "react-responsive-carousel/lib/styles/carousel.min.css";
import {Carousel} from 'react-responsive-carousel'
import {Link} from "react-router-dom";

const pics = [
	"http://lorempixel.com/output/cats-q-c-640-480-3.jpg"
]

class ArtworkPage extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			artwork: null,
			photos: pics,
			artist: null,
		}
	}

	componentDidMount() {
		console.log(this.props.match.params.id)
		fetch("https://mem-ksa.herokuapp.com/api/artworks/?format=json")
			.then(response => {
				if (response.ok) {
					return response.json();
				} else {
					throw new Error('Something went wrong ...');
				}
			})
			.then(data => {
				console.log(data)
				for (let i in data.results) {
					console.log(i)
					if (data.results[i].id == this.props.match.params.id) {
						let artwork = data.results[i]
						this.setState({artwork: artwork})
						return artwork
					}
				}
			}).then((data) => {
			let photos = data.photos
			console.log(photos)
			Promise.all(photos.map((p) => fetch(p))).then(data =>
				Promise.all(data.map(res => res.text()))).then(imgs => {
				let ph = this.state.photos
				const mas = imgs.map(e => JSON.parse(e).image)
				ph = []
				ph.push(...mas);
				console.log(ph)
				this.setState({photos: ph})
			})
		}).catch(error => console.log(error));

		fetch("https://mem-ksa.herokuapp.com/api/artworks/?format=json")
			.then(response => {
				if (response.ok) {
					return response.json();
				} else {
					throw new Error('Something went wrong ...');
				}
			})
			.then(data => {
				console.log("HERE")
				for (let i in data.results) {
					console.log(i)
					if (data.results[i].id == this.props.match.params.id) {
						let artwork = data.results[i]
						return artwork
					}
				}
			}).then((data) => {
			console.log(data.author)
			return fetch(data.author)
		}).then((data) => {
			console.log(data)
			return data.json()
		}).then((data) => {
			let artist = data
			this.setState({artist: artist})
		}).catch(error => console.log(error));
	}

	render() {
		let image_url = this.state.artwork
		let artist
		if (this.state.artist === null) {
			artist = <h1 id="artist-name">Unknown</h1>
		} else {
			artist =
				<Link to={`/artist/${this.state.artist.id}`}>
					<h1 id="artist-name">{this.state.artist.name}</h1>
					<h2 id="artist-name">{this.state.artist.nick}</h2>
				</Link>
		}
		return <div className="App-main">
			{image_url !== null && image_url !== undefined &&
			<h1 id="artwork-name" className="center">{image_url.name}</h1>}
			{/* PLACEHOLDER DELETE WHEN REPLACING WITH CONTENT THAT WORKS */}
			<Carousel dynamicHeight infiniteLoop>
				{this.state.photos? this.state.photos.map((photo)=> {
					console.log(photo)
					return <img src={photo} ></img>}) : ''}
			</Carousel>
			{artist}
			{image_url !== null && image_url !== undefined && <p id="artwork-description">{image_url.description}</p>}
			<Tags tags={['Стріт-арт']}></Tags>
		</div>
	}

	/*
	{{this.state.photos.map((photo) => {
				return <img src={photo}></img>})}


	 */
}

export default ArtworkPage;