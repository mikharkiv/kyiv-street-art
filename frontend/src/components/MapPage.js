import React from 'react';
import { Map, Marker, Popup, TileLayer } from 'react-leaflet'
import L from "leaflet"
import MarkerPopUp from "./MarkerPopUp";

delete L.Icon.Default.prototype._getIconUrl;

L.Icon.Default.mergeOptions({
	iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
	iconUrl: require('leaflet/dist/images/marker-icon.png'),
	shadowUrl: require('leaflet/dist/images/marker-shadow.png')
});

const location = [50.404613, 30.679534]

class MapPage extends React.Component {
	state = { arts:[] };

	componentDidMount() {
		fetch("https://mem-ksa.herokuapp.com/api/artworks/?format=json")
			.then(response => {
				if (response.ok) {
					return response.json();
				} else {
					throw new Error('Something went wrong ...');
				}
			})
			.then(data => {
				console.log(data.results)
				this.setState({ arts: data.results})
			})
			.catch(error => this.setState({ error, isLoading: false }));
	}

	render() {
		return (
			<div className="App-main">
				<h1 className="center">Street Art Map</h1>
				<Map id="mapid" center={location} zoom={12}>
					<TileLayer
						url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
						attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
					/>
					{this.state.arts.map((art) =>{
						return <Marker key={art.id} position={[art.location_x,art.location_y]}>
							<Popup><MarkerPopUp art={art} /></Popup>
						</Marker>
					})}
				</Map>
				<div>
					<h2>Карта Київського Стріт-Арту</h2>
					<p>Вуличне мистецтво це той вид мистецтва, що оточує нас усюди. Попри його особливості, варто визнати, що він є важливою культурною складовою. Та, на жаль, іноді цей вид мистецтва руйнується та ним нехтують. Тому ми створили карту вуличного мистецтва. Насолоджуйтеся!</p>
				</div>
			</div>
		);
	}
}

export default MapPage;
