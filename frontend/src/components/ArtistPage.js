import React from 'react';
import Tags from './Tags'
import Artworks from './Artworks'

const artworks = [
	{
		name: 'Інструменти',
		image: "https://unexploredcity.com/uploads/tour_points/60/35/vw-dj3g0a25k_26629b4b.jpg",
		description: 'Одна з перших робот Гамлета. Дуже круто. Вражає. Потрясно. Неймовірно. Lorem Ipsum ля-ля-ля.'
	},
	{
		name: 'Долоня',
		image: "https://i.imgur.com/fs6mbOo.jpg",
		description: 'Одна з перших робот Гамлета. Дуже круто. Вражає. Потрясно. Неймовірно. Lorem Ipsum ля-ля-ля.'
	}
]

class ArtistPage extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
				artist_id: 0,
				artist_image: '',
				artist_name:'',
				artist_real_name:'',
				artist_description:'',
				artist_tags:[],
				artworks:[]
		}
	}

	componentDidMount() {
		let url = `https://mem-ksa.herokuapp.com/api/authors/${this.props.match.params.id}/?format=json`;
		fetch(url).then(data => {
			return data.json()
		}).then(res=> {
			this.setState({
				artist_id: res.id,
				artist_name: res.nick,
				artist_real_name:res.name,
				artist_description: res.description
			})
			const new_url = `https://mem-ksa.herokuapp.com/api/artworks/?format=json`;

			fetch(new_url).then(data => {
				return data.json()
			}).then(res=> {
				let author_work = res.results.filter(e=> e.author.includes( url))
				console.log(author_work);
				for (let i in author_work) {
					let work = author_work[i];
					let arts = this.state.artworks.slice();
					arts.push({name: work.name,
										description: work.description})
					this.setState({artworks: arts})
				}
				return author_work;
			}).then(data => {
				let photo0 = data.map(e=>e.photos[0])
				console.log(photo0)
				Promise.all(photo0.map(e=>fetch(e).
				then(res=> res.json()))).
				then(res=> {
					let imgs = res.map(e=> e.image)
					let arts = this.state.artworks.slice();
					for (let i = 0; i < imgs.length; i++) {
						arts[i].image = imgs[i];
					}
					console.log(arts);
					this.setState({artworks: arts});
				});
			})

		})




	}

	render() {
		return <div className="App-main">
			<h1 className="center" >Художник</h1>
			<div className="artist-info-container">
				<img id="artist-pic" src="http://via.placeholder.com/400" alt="Artist" />
				<div>
					<h1 id="artist-name">{this.state.artist_name}</h1>
					<span id="artist-real-name">{this.state.artist_name}</span>
				</div>
			</div>
			<p id="artist-description">{this.state.artist_name}.</p>
			<Tags tags={['Стріт-арт', 'Сучасне мистецтво', 'Графіті']} />
			<h1>Роботи</h1>
			<Artworks arts={this.state.artworks} />
		</div>
	}
}

export default ArtistPage