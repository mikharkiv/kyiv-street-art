import React from 'react';


class Footer extends React.Component {
	render() {
		return (
			<footer className="App-footer">
				<p>
					Need help? | Developed by Michael Postnikov, Kyrylo Turina,
				Vadym Nakytnyak, 2020 | <a href="https://gitlab.com/mikharkiv/kyiv-street-art" target="_blank" className="link">
						GitLab page
					</a>
				</p>
			</footer>
		);
	}
}

export default Footer;