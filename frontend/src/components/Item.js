import React from 'react';
import "../styles/Gallery.css"
import {Link} from "react-router-dom";

class Item extends React.Component {
	constructor(props) {
		super(props);
		this.state={
			photo: ['https://i.imgur.com/GONyWCm.png', 'https://i.imgur.com/fs6mbOo.jpg'],
		}
	}

	componentDidMount() {
		if (this.props.art.photos !== undefined)
			fetch(this.props.art.photos[0])
				.then(response => {
					if (response.ok) {
						return response.json();
					} else {
						throw new Error('Something went wrong ...');
					}
				})
				.then(data => {
					this.setState({photo: data.image})
				})
				.catch(error => this.setState({error, isLoading: false}));
	}

	render() {
		return (<div className="Item">
			<Link to={`/artwork/${this.props.art.id}`}>
				<img width='250px' height='250px' src={this.state.photo}></img>
			</Link>
			<p>{this.props.art.name}</p>
		</div>);
	}
}

export default Item