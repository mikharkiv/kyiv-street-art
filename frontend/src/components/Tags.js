import React from 'react'

class Tags extends React.Component {
	render() {
	const elementsList = this.props.tags.map( (e) => {
		return <div key={e} className="tag">	{e}</div>
	})
	return <div id="tags">
		{elementsList}	
		</div>
	}
}


export default Tags