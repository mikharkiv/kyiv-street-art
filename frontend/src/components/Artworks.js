import React from 'react'

function Artworks(props) {
	return <div className="artworks-list">
		{props.arts.map((e) => {
			return <div id='artist-page-work'>
				<div id="artist-artwork-container">
					<div id="artist-artwork" style={{ backgroundImage: `url("${e.image}"` }}></div>
				</div>
				<div id="artist-artwork-desc-container">
					<h1 id="artist-artwork-name">{e.name}</h1>
					<span id="artist-artwork-desc">{e.description}</span>
				</div>
			</div>
		})}
	</div>
}

export default Artworks