import React from 'react';
import StackGrid from "react-stack-grid";
import Item from "./Item"
import '../styles/Gallery.css'

class GalleryPage extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			arts: [],
		};
	}

	componentDidMount() {
		fetch("https://mem-ksa.herokuapp.com/api/artworks/?format=json")
			.then(response => {
				if (response.ok) {
					return response.json();
				} else {
					throw new Error('Something went wrong ...');
				}
			})
			.then(data => {
				console.log(data)
				this.setState({ arts: data.results})
			})
			.catch(error => this.setState({ error, isLoading: false }));
		console.log(this.state.arts)
	}

	render() {
		return (
			<div className="App-main">
				<StackGrid
					columnWidth={300}
				>
					{this.state.arts.map((art) => {
						return <Item art={art} key={art.id} />
					})}
				</StackGrid>
			</div>)
	}
}

export default GalleryPage