import React from 'react';
import {Link} from "react-router-dom";

class MarkerPopUp extends React.Component{

	constructor(props) {
		super(props);
		this.state={
			photo:null,
		}
	}

	componentDidMount() {
		if (this.props.art.photos !== undefined)
			fetch(this.props.art.photos[0])
				.then(response => {
					if (response.ok) {
						return response.json();
					} else {
						throw new Error('Something went wrong ...');
					}
				})
				.then(data => {
					this.setState({photo: data.image})
				})
				.catch(error => this.setState({error, isLoading: false}));
	}

	render() {
		return (
			<div>
				<b>Title: </b>{this.props.art.name}<br/>
				<b>Description:: </b>{this.props.art.description}<br/>
				<b>Location Description: </b>{this.props.art.location_x} {this.props.art.location_y}<br/>
				<Link to={`/artwork/${this.props.art.id}`}>
					<img width='100px' height='100px' src={this.state.photo}></img>
				</Link>
			</div>)
	}
}

export default MarkerPopUp;