import React from 'react';
import {Link} from "react-router-dom";

function NavBar(){
	return <div className="NavBar">
		<Link className="header-links " to="/">Головна</Link>
		{/* <Link className="header-links" to="/map">Карта</Link> */}
		<Link className="header-links" to="/gallery">Галерея</Link>
	</div>
}

export default NavBar