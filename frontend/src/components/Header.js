import React from 'react';
import NavBar from './NavBar'

class Header extends React.Component {
	render() {
		return (
			<header className="App-header">
				<a className="noselect" id="logo" href="./" >KSA</a>
				<NavBar/>
			</header>
		);
	}
}

export default Header;