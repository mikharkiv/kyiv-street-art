import os
from django.db import models
from datetime import datetime
from rest_framework import serializers


class AuthorImage(models.Model):

	def get_upload_path(self, filename):
		return "author/%s/images/" % ( str(datetime.now().strftime("%Y-%m-%d_%H-%M-%S"))) + filename

	id = serializers.ReadOnlyField()
	image = models.ImageField(upload_to=get_upload_path)


class Author(models.Model):
	id = serializers.ReadOnlyField()
	nick = models.CharField(max_length=30, unique=True, default='')
	name = models.CharField(max_length=70, unique=True, blank=True)
	description = models.TextField(blank=True)
	photos = models.ForeignKey('AuthorImage', on_delete=models.CASCADE, null=True, blank=True)
