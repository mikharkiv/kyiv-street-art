from rest_framework import serializers
from .models import *

class AuthorSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = Author
		fields=('id', 'nick', 'name','description','photos',)


class AuthorImageSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = AuthorImage
		fields = ('id', 'image',)
