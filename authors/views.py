from rest_framework import viewsets
from django.shortcuts import render
from .models import *
from .serializers import *


class AuthorViewSet(viewsets.ModelViewSet):
	queryset = Author.objects.all()
	serializer_class = AuthorSerializer


class AuthorImageSet(viewsets.ModelViewSet):
	queryset = AuthorImage.objects.all()
	serializer_class = AuthorImageSerializer